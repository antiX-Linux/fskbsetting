#!/usr/bin/python
# coding: utf-8
#
# setup.py
# Copyright (C) FSnow 2012-2017 <fsnow@yandex.ru>
# 
# fskbsetting is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# fskbsetting is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

import glob
from DistUtilsExtra.auto import setup

setup(name='fskbsetting',
      version='0.5.3',
      description='Configures keyboard options and layouts.',
      author='FSnow',  
      author_email='fsnow@yandex.ru',
      url='http://forum.runtu.org/',
      data_files=[
          ('share/fskbsetting/flags/', glob.glob('data/flags/*.png')),
          ('share/fskbsetting/flags/nec_vndr/', glob.glob('data/flags/nec_vndr/*.png')),
          ('share/fskbsetting/daemon/', ['fskbsetting/daemon/fskbsetting-daemon']),
          ],
      license='GPL3',
      platforms='linux',
)
