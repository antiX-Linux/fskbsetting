__all__ = (
        'PACKAGE',
        'VERSION',
        'DATADIR',
        )

import os
import gi
gi.require_version('GLib', '2.0')
from gi.repository import GLib

PACKAGE = 'fskbsetting'
VERSION = '0.5.3'
DATADIR = '/usr/share/%s/'%(PACKAGE)
IS_INSTALLED = True

if not __file__.startswith('/usr'):
    datadir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    DATADIR = os.path.join(datadir, 'data')
    IS_INSTALLED = False
