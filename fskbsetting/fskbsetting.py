#!/usr/bin/env python
# coding: utf-8
#
# fskbsetting.py
# Copyright (C) FSnow 2009-2017 <fsnow@yandex.ru>
#
# fskbsetting is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fskbsetting is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import re
import lxml.etree
import dbus
import subprocess
import locale
import gettext
import gi
gi.require_version('GLib', '2.0')
gi.require_version('Gtk', '3.0')
gi.require_version('GdkPixbuf', '2.0')
from gi.repository import GLib, Gtk, GdkPixbuf
from configobj import ConfigObj
from consts import PACKAGE, VERSION, DATADIR

# Internationalization
locale.setlocale(locale.LC_ALL, '')
gettext.textdomain(PACKAGE)
_ = gettext.gettext

########################################################################
def XkeyboardTranslate(message):
    return gettext.dgettext('xkeyboard-config', message)


########################################################################
# main window
class fsKBSetting(Gtk.Builder):
    def __init__(self):
        super(fsKBSetting, self).__init__()

        self.set_translation_domain(PACKAGE)
        self.add_from_file('%s/ui/%s.ui' % (DATADIR, PACKAGE))
        self.lstore_layout.set_sort_column_id(2, Gtk.SortType.ASCENDING)

        self._quit = False
        self._changed = False
        self._changed_numlockx = False
        self._still_working = False
        self.lstore_variants = {}

        self.load_xkb_base()
        self.load_default_keyboard()
        self.load_default_numlockx()

        self.update_ui()
        self.connect_signals(self)
        self.window_main.show_all()

    #-------------------------------------------------------------------
    def __getattr__(self, attr):
        obj = self.get_object(attr)
        if not obj:
            raise AttributeError('object %r has no attribute %r' % (self,attr))
        setattr(self, attr, obj)
        return obj

    #-------------------------------------------------------------------
    def update_ui(self, widget=None, data=None):
        if self._changed == True or self._changed_numlockx == True:
            self.btn_apply.set_sensitive(True)
        else:
            self.btn_apply.set_sensitive(False)

        if len(self.lstore_selected_layouts)>3:
            self.btn_add.set_sensitive(False)
        else:
            self.btn_add.set_sensitive(True)

        selection = self.tree_selected_layouts.get_selection()
        model, iter = selection.get_selected()

        if iter is None or len(self.lstore_selected_layouts)<2:
            self.btn_up.set_sensitive(False)
            self.btn_down.set_sensitive(False)
            self.btn_del.set_sensitive(False)
        else:
            if model.iter_previous(iter) is None:
                self.btn_up.set_sensitive(False)
            else:
                self.btn_up.set_sensitive(True)

            if model.iter_next(iter) is None:
                self.btn_down.set_sensitive(False)
            else:
                self.btn_down.set_sensitive(True)

            self.btn_del.set_sensitive(True)
        
        if not self.has_numlockx():
            self.label_numlockx.set_sensitive(False)
            self.combo_numlockx.set_sensitive(False)
            self.label_numlockx.set_tooltip_markup("Install <b>numlockx</b> to activate this")
            self.combo_numlockx.set_tooltip_markup("Install <b>numlockx</b> to activate this")
    
    #-------------------------------------------------------------------
    def has_numlockx(self):
        return os.access("/usr/bin/numlockx", os.X_OK)

    #-------------------------------------------------------------------
    def load_xkb_base(self):
        tree = lxml.etree.parse('/usr/share/X11/xkb/rules/base.xml')

        models = tree.xpath('//model')
        for model in models:
            modelName = model.xpath('./configItem/name')[0].text
            modelDesc = model.xpath('./configItem/description')[0].text
            self.lstore_xkbmodel.append([modelName,
                                         XkeyboardTranslate(modelDesc)])

        layouts = tree.xpath('//layout')
        for layout in layouts:
            layoutName = layout.xpath('./configItem/name')[0].text
            layoutDesc = layout.xpath('./configItem/description')[0].text

            self.lstore_layout.append([self.get_layout_flag(layoutName),
                                       layoutName,
                                       XkeyboardTranslate(layoutDesc)])
            self.lstore_variants[layoutName] = None

            for variant in layout.xpath('./variantList/variant'):
                variantName = variant.xpath('./configItem/name')[0].text
                variantDesc = variant.xpath('./configItem/description')[0].text
                if self.lstore_variants[layoutName] == None:
                    self.lstore_variants[layoutName] = Gtk.ListStore(str, str)
                    self.lstore_variants[layoutName].append(["", ""])
                    self.lstore_variants[layoutName].set_sort_column_id(1, Gtk.SortType.ASCENDING)
                self.lstore_variants[layoutName].append([variantName, XkeyboardTranslate(variantDesc)])

        regex_toggle = re.compile(r"^grp:.*toggle$")
        regex_lv3 = re.compile(r"^lv3:.*_switch$")
        regex_compose = re.compile(r"^compose:.*$")
        regex_led = re.compile(r"^grp_led:.*$")

        options = tree.xpath('//option')
        for option in options:
            optionName = option.xpath('./configItem/name')[0].text
            if regex_toggle.match(optionName):
                optionDesc = option.xpath('./configItem/description')[0].text
                self.lstore_keys_layout.append([optionName, XkeyboardTranslate(optionDesc)])
            elif regex_lv3.match(optionName):
                optionDesc = option.xpath('./configItem/description')[0].text
                self.lstore_keys_lv3.append([optionName, XkeyboardTranslate(optionDesc)])
            elif regex_compose.match(optionName):
                optionDesc = option.xpath('./configItem/description')[0].text
                self.lstore_keys_compose.append([optionName, XkeyboardTranslate(optionDesc)])
            elif regex_led.match(optionName):
                optionDesc = option.xpath('./configItem/description')[0].text
                self.lstore_grp_led.append([optionName, XkeyboardTranslate(optionDesc)])
                
        self.lstore_keys_layout.set_sort_column_id(1, False)
        self.lstore_keys_compose.set_sort_column_id(1, False)
        self.lstore_keys_lv3.set_sort_column_id(1, False)
        self.lstore_grp_led.set_sort_column_id(1, False)

    #-------------------------------------------------------------------
    def load_default_keyboard(self):
        console_setup = file('/etc/default/keyboard')
        config = ConfigObj(console_setup)

        xkbmodel = config['XKBMODEL']
        xkblayout = config['XKBLAYOUT'].split(',')
        xkbvariant = config['XKBVARIANT'].split(',')
        xkboptions = config['XKBOPTIONS'].split(',')

        self.combo_xkbmodel.set_active_id(xkbmodel)

        for l, v in zip(xkblayout, xkbvariant):
            self.lstore_selected_layouts.append([
                self.get_layout_flag(l),
                l,
                self.get_layout_desc(l),
                v,
                self.get_variant_desc(l, v)])

        for option in xkboptions:
            if option.startswith('grp:') and option.endswith('toggle'):
                self.combo_hotkey_switch.set_active_id(option)
            elif option.startswith('grp_led:'):
                self.combo_indicator.set_active_id(option)
            elif option.startswith('compose:'):
                self.combo_hotkey_compose.set_active_id(option)
            elif option.startswith('lv3:'):
                self.combo_lv3.set_active_id(option)
            elif option.startswith('terminate:ctrl_alt_bksp'):
                self.check_terminate.set_active(True)

    #-------------------------------------------------------------------
    def load_default_numlockx(self):
        if self.has_numlockx() and os.access('/etc/default/numlockx', os.R_OK):
            numlockx_default = file('/etc/default/numlockx')
            config = ConfigObj(numlockx_default)
            if config.has_key('NUMLOCK'):
                self.combo_numlockx.set_active_id(config['NUMLOCK'])

    #-------------------------------------------------------------------
    def set_default_keyboard(self, widget=None):
        xkbmodel = ''
        xkblayout = ''
        xkbvariant = ''
        xkboptions = ''

        if self.combo_xkbmodel.get_active() >= 0:
            model = self.lstore_xkbmodel
            iter = model.get_iter(self.combo_xkbmodel.get_active())
            xkbmodel = model.get_value(iter, 0)

        for row in self.lstore_selected_layouts:
            xkblayout += '%s,'%(row[1])
            xkbvariant += '%s,'%(row[3])
        xkblayout = xkblayout[:-1]
        xkbvariant = xkbvariant[:-1]

        if self.combo_hotkey_switch.get_active() >= 0:
            model = self.lstore_keys_layout
            iter = model.get_iter(self.combo_hotkey_switch.get_active())
            xkboptions += '%s,'%model.get_value(iter, 0)

        if self.combo_hotkey_compose.get_active() > 0:
            model = self.lstore_keys_compose
            iter = model.get_iter(self.combo_hotkey_compose.get_active())
            xkboptions += '%s,'%model.get_value(iter, 0)

        if self.combo_lv3.get_active() > 0:
            model = self.lstore_keys_lv3
            iter = model.get_iter(self.combo_lv3.get_active())
            xkboptions += '%s,'%model.get_value(iter, 0)

        if self.combo_indicator.get_active() > 0:
            model = self.lstore_grp_led
            iter = model.get_iter(self.combo_indicator.get_active())
            xkboptions += '%s,'%model.get_value(iter, 0)

        if self.check_terminate.get_active():
            xkboptions += 'terminate:ctrl_alt_bksp,'

        xkboptions = xkboptions[:-1]

        bus = dbus.SystemBus()
        remote_object = bus.get_object("fskbsetting.Daemon", "/fskbsettingDaemon")
        self.iface = dbus.Interface(remote_object, "fskbsetting.SampleInterface")
        self.iface.set_default_keyboard(xkbmodel, xkblayout, xkbvariant, xkboptions)

        self.notebook_main.set_sensitive(False)
        self.box_bottom.hide()
        self.progress.show()
        self._changed = False
        self.update_ui()

        GLib.timeout_add(100, self.pulse)

    #-------------------------------------------------------------------
    def set_default_numlockx(self, widget=None):
        if self.combo_numlockx.get_active() >= 0:
            model = self.lstore_numlockx
            iter = model.get_iter(self.combo_numlockx.get_active())
            numlock = model.get_value(iter, 0)

            bus = dbus.SystemBus()
            remote_object = bus.get_object("fskbsetting.Daemon", "/fskbsettingDaemon")
            self.iface = dbus.Interface(remote_object, "fskbsetting.SampleInterface")
            self.iface.set_default_numlockx(numlock)
            self.iface.Exit()

            if numlock in ['on', 'off', 'toggle']:
                subprocess.Popen('numlockx %s' % numlock, shell=True)

        self._changed_numlockx = False
        self.update_ui()
    
    #-------------------------------------------------------------------
    def pulse(self):
        self._still_working = self.iface.still_working()
        if self._still_working:
            self.progress.pulse()
            return True
        else:
            self.iface.Exit()
            self.notebook_main.set_sensitive(True)
            self.progress.hide()
            self.box_bottom.show()
            if self._quit:
                self.on_quit()

    #-------------------------------------------------------------------
    def on_btn_apply_clicked(self, widget):
        if self._changed:
            self.set_default_keyboard()

        if self._changed_numlockx:
            self.set_default_numlockx()

    def on_btn_ok_clicked(self, widget):
        if self._changed_numlockx:
            self.set_default_numlockx()
        if self._changed:
            self._quit = True
            self.set_default_keyboard()
        else:
            self.on_quit()

    #-------------------------------------------------------------------
    def get_layout_flag(self, layout):
        try:
            pixbuf = GdkPixbuf.Pixbuf.new_from_file('%s/flags/%s.png'%(DATADIR, layout))
        except:
            pixbuf = GdkPixbuf.Pixbuf.new_from_file('%s/flags/none.png'%(DATADIR))
        return pixbuf

    def get_layout_desc(self, layout):
        for row in self.lstore_layout:
            if row[1] == layout:
                return row[2]
        return None

    def get_variant_desc(self, layout, variant):
        for row in self.lstore_variants[layout]:
            if row[0] == variant:
                return row[1]
        return None

    #-------------------------------------------------------------------
    def on_btn_add_clicked(self, widget):
        self.dialog_layout_new.run()

    def on_dialog_layout_new_response(self, widget, response=None):
        if response == 1:
            model = self.lstore_layout
            iter = model.get_iter(self.combo_layout.get_active())
            layout_pixbuf = model.get_value(iter, 0)
            layout_name = model.get_value(iter, 1)
            layout_desc = model.get_value(iter, 2)

            model = self.lstore_variants[layout_name]
            if model is not None:
                iter = model.get_iter(self.combo_variant.get_active())
                variant_name = model.get_value(iter, 0)
                variant_desc = model.get_value(iter, 1)
            else:
                variant_name = ""
                variant_desc = ""

            self.lstore_selected_layouts.append([
                layout_pixbuf,
                layout_name,
                layout_desc,
                variant_name,
                variant_desc])

            self.on_changed()

        self.dialog_layout_new.hide()

    #-------------------------------------------------------------------
    def on_btn_up_clicked(self, widget):
        selection = self.tree_selected_layouts.get_selection()
        model, iter = selection.get_selected()

        if iter is None:
            return
        elif model.iter_previous(iter) is not None:
            model.swap(iter, model.iter_previous(iter))

        self.on_changed()

    #-------------------------------------------------------------------
    def on_btn_down_clicked(self, widget):
        selection = self.tree_selected_layouts.get_selection()
        model, iter = selection.get_selected()

        if iter is None:
            return
        elif model.iter_next(iter) is not None:
            model.swap(iter, model.iter_next(iter))

        self.on_changed()

    #-------------------------------------------------------------------
    def on_btn_del_clicked(self, widget):
        selection = self.tree_selected_layouts.get_selection()
        model, iter = selection.get_selected()

        if iter is None: return

        model.remove(iter)
        self.on_changed()

    #-------------------------------------------------------------------
    def on_combo_layout_changed(self, widget):
        model = widget.get_model()
        iter = model.get_iter(widget.get_active())
        layout = model.get_value(iter, 1)

        self.combo_variant.set_model(self.lstore_variants[layout])
        self.combo_variant.set_active(0)
        self.btn_dlg_add.set_sensitive(self.combo_layout.get_active()>=0)

    #-------------------------------------------------------------------
    def on_changed(self, widget=None, data=None):
        self._changed = True
        self.update_ui()

    def on_changed_numlockx(self, widget=None, data=None):
        self._changed_numlockx = True
        self.update_ui()

    #-------------------------------------------------------------------
    def on_about(self, widget):
        self.dialog_about.set_version(VERSION)
        self.dialog_about.run()

    def on_about_response(self, widget, data=None):
        self.dialog_about.hide()

    #-------------------------------------------------------------------
    def on_quit(self, widget=None):
        if self._still_working:
            self._quit = True
        else:
            Gtk.main_quit()


def main():
    app = fsKBSetting()
    Gtk.main()

if __name__ == "__main__":
    sys.exit(main())
